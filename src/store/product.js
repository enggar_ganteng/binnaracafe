import {action, observable} from "mobx";
import {http} from "../utils/http";

export class Product {

    constructor(context) {
        this.context = context;
    }

    @observable data = [] ;

    @action
    async getProduct() {
        console.log('get product')
        const res = await http.get("/product");
        console.log('res product', res.body);
        this.data = res.body;
    };

    @action
    async insertProduct(data) {
        console.log('insertProduct coy', data)
        const res = await http.post("/product").send(data);
        this.getProduct();
        console.log('res insertProduct coy', res);
    };

    @action
    async deleteProduct(id) {
        console.log('deleteProduct', id)
        const res = await http.post(`/product/${id}`);
        this.getProduct();
        console.log('res deleteProduct coy', res);
    };
}
