import {action, observable} from "mobx";
import * as Centrifuge from "centrifuge";

export class Chat {
    @observable chatMessages = [];

    centrifuge;

    @action
    initialize() {
        this.centrifuge = new Centrifuge('wss://kesan-api.bangun-kreatif.com/centrifugo/connection/websocket');
        this.centrifuge.setToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJheWF5YSIsImV4cCI6MTY0NjQwMzk0MX0.iITm3BPT-ZZCvXPc0D0jKlb46xfAbMMG4PnF1iGSjmY");
    }

    @action
    connect() {
        this.centrifuge.on('connect', function(ctx){
            console.log('Connected over ' + ctx.transport);
        });
        this.centrifuge.on('disconnect', function(ctx){
            console.log('Disconnected: ' + ctx.reason);
        });

        this.centrifuge.subscribe("chat", action((ctx) => {
            this.chatMessages.push(ctx.data);
        }))
        this.centrifuge.connect();
    }

    @action
    subscribe() {
    }

    @action
    async sendMessage(message){
        const ini = await this.centrifuge.publish("chat", {"id": Math.random().toString(), "message": message, "user_id": "cms"});
        console.log("ini", ini);
    }
}
