import {action, observable} from "mobx";
import {http} from "../utils/http";

export class Authentication {

    constructor(context) {
        this.context = context;
    }

    @action
    logout() {
        localStorage.setItem('id_token', '');
    }

    @action
    login(token) {
        localStorage.setItem('id_token', token);
    }
}
