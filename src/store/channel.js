import {action, observable} from "mobx";
import {http} from "../utils/http";

export class Channel {
    @observable
    channels = [{
        id: '123123',
        message: 'start'
    }];

    @action
    async getChannel() {
        const response = await http.get("/v1/chat_room");
        this.channels = response.body.data;
    };

    @action
    tesuto() {
        this.channels.push({id: Math.random(), message: 'ayayay'});
    }
}
