import {action, observable} from "mobx";
import {http} from "../utils/http";

export class Order {

    constructor(context) {
        this.context = context;
    }

    @observable dataCart = [] ;
    @observable dataOrder = [] ;

    @action
    async getCart() {
        console.log('get cart')
        const res = await http.get("/cart");
        console.log('res cart', res.body);
        this.dataCart = res.body;
    };

    @action
    async insertCart(data) {
        console.log('insertCart', data)
        const res = await http.post("/cart").send(data);
        console.log('res insertCart', res);
    };

    @action
    async checkoutCart(id) {
        console.log('checkoutCart', id)
        const res = await http.put(`/checkout/${id}`);
        this.getCart();
        console.log('res checkoutCart', res);
    };

    @action
    async getOrder() {
        console.log('get getOrder')
        const res = await http.get("/order-list");
        console.log('res getOrder', res.body.result);
        console.log('res getOrder 2', res.body.result[0].order_list);
        this.dataOrder = res.body.result[0].order_list;
    };
}
