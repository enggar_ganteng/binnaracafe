import {observable, computed} from "mobx";
import {UI} from "./ui";
import {Channel} from "./channel";
import {Authentication} from "./authentication";
import {Product} from "./product";
import {Chat} from "./chat";
import {Order} from "./order";

export class Store {
    @observable
    accessToken = '';

    @observable
    refreshToken = '';

    channel = new Channel();
    authentication = new Authentication();
    product = new Product();
    ui = new UI();
    chat = new Chat();
    order = new Order();

    @computed
    get isLoggedIn() {
        return !!this.refreshToken;
    }

    setInitialToken(accessToken, refreshToken) {
        this.setToken(accessToken, refreshToken);
    }

    setToken(accessToken, refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }
}
