const packageJson = require('../../package.json');

export const appConfig = {
    // apiUrl: "https://kesan-api.bangun-kreatif.com",
    apiUrl: "http://localhost:8000",
    apiFileUrl: "http://localhost:3001",
    version: packageJson.version
};
