import {Redirect, Route, Switch} from "react-router-dom";
import {Login} from "../pages/Login/Login";
import {PublicRoute} from "../component/PublicRoute";
import {PrivateRoute} from "../component/PrivateRoute";
import {App} from "../pages/App/App";

export const MainRoutes = (props) => {

    const PrivateRoute = ({component: Component, ...rest}) => {
        return (
            <Route
                {...rest}
                render={(props) => {
                    return !!localStorage.getItem('id_token') && localStorage.getItem('id_token') !== 'null'
                        ? (<Component {...props}/>)
                        : ((<Redirect to={{pathname: '/login'}}/>)
                        );
                }}/>
        )
    }

    return (
        <Switch>
            <Route path="/" exact>
                <Redirect to={"/app/home"} />
            </Route>
            <PublicRoute restricted={true} component={Login} path="/login" exact />
            <PrivateRoute path="/app" component={App}/>
            {/*<PublicRoute component={App} path="/app" />*/}
        </Switch>
    );
};
