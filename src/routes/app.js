import {Redirect, Route, Switch} from "react-router-dom";
import {PublicRoute} from "../component/PublicRoute";
import {Login} from "../pages/Login/Login";
import {App} from "../pages/App/App";
import {Home} from "../pages/Home/Home";
import {About} from "../pages/About/About";
import {SimulateChat} from "../pages/SimulateChat/SimulateChat";
import {Kiai} from "../pages/Kiai/Kiai";
import {ProductList} from "../pages/ProductList";
import {MenuList} from "../pages/MenuList";
import {Cart} from "../pages/Cart";
import {Order} from "../pages/Order";
import {OrderDetail} from "../pages/OrderDetail";
import {FormProduct} from "../pages/FormProduct";

export const AppRoute = () => {
    return <Switch>
        <Route path={"/app/home"}>
            <Home/>
        </Route>
        <Route path={"/app/kiai"}>
            <Kiai/>
        </Route>
        <Route path={"/app/simulate_chat"}>
            <SimulateChat/>
        </Route>
        <Route exact path={"/app/product"}>
            <ProductList/>
        </Route>
        <Route path={"/app/menu"}>
            <MenuList/>
        </Route>
        <Route path={"/app/cart"}>
            <Cart/>
        </Route>
        <Route exact path={"/app/order"}>
            <Order/>
        </Route>
        <Route path={"/app/order/:id"}>
            <OrderDetail/>
        </Route>
        <Route path={"/app/product/form"}>
            <FormProduct/>
        </Route>
        <Route path="/app" exact>
            <Redirect to={'/app/home'} />
        </Route>
    </Switch>
}
