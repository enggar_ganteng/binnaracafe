import React, {useEffect, useState} from "react";
import {Avatar, Button, Card, Col, PageHeader, Row, Space, Table, notification, InputNumber, message, Typography, Modal, Form, Input, Select, Upload} from "antd";
import {observer} from "mobx-react-lite";
import {useStore} from "../../utils/useStore";
import {PlusOutlined, LoadingOutlined, ExclamationCircleOutlined} from '@ant-design/icons';
import NumberFormat from "react-number-format";
import { useHistory } from "react-router-dom";

const { Option } = Select;
const { Title } = Typography;
const {Meta} = Card;

export const FormProduct = observer(() => {
    const store = useStore();
    const history = useHistory();
    const [form] = Form.useForm();
    const [data, setData] = useState([]);
    const { loading, setLoading } = useState(false);
    const { imageUrl, setImageUrl } = useState(false);
    const [initialData] = useState({});

    const setFieldsValue = () => {
        form.setFieldsValue({
            name: '',
            category: '',
            stock: '',
            price: '',
            photo: '',
        });
    };

    useEffect(() => {
        const init = async () => {

        };
        init();
        setFieldsValue();
    }, []);

    const uploadButton = (
        <div>
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div style={{ marginTop: 8 }}>Upload</div>
        </div>
    );

    const beforeUpload = (file) => {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error('You can only upload JPG/PNG file!');
        }
        const isLt2M = file.size / 1024 / 1024 < 2;
        if (!isLt2M) {
            message.error('Image must smaller than 2MB!');
        }
        return isJpgOrPng && isLt2M;
    }

    const getBase64 = (img, callback) => {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    }

    const   handleChange = info => {
        if (info.file.status === 'uploading') {
            setLoading(true)
            return;
        }
        if (info.file.status === 'done') {
            // Get this url from response in real world.
            getBase64(info.file.originFileObj, imageUrl =>
                this.setState({
                    imageUrl,
                    loading: false,
                }),
            );
        }
    };

    return <div>
        <PageHeader
            style={{
                padding: 0,
                margin: 0,
                height: 40,
                backgroundColor: "transparent",
            }}
            title={"Form Product"}
        >
        </PageHeader>
        <Card>
            <div>
                <Form
                    layout={'vertical'}
                    form={form}
                    initialValues={initialData}
                >
                    <Form.Item
                        name="name"
                        label="Name"
                        rules={[
                            {
                                required: true,
                                message: 'Nama tidak boleh kosong!',
                            },
                        ]}
                    >
                        <Input placeholder="input Name" />
                    </Form.Item>
                    <Form.Item
                        name="category"
                        label="Category"
                        rules={[
                            {
                                required: true,
                                message: 'Category tidak boleh kosong!',
                            },
                        ]}
                    >
                        <Select placeholder="Input Category" style={{ width: '100%' }}>
                            <Option value="food">Food</Option>
                            <Option value="drink">Drink</Option>
                            <Option value="snack">Snack</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item
                        name="stock"
                        label="Stock"
                        rules={[
                            {
                                required: true,
                                message: 'Stock tidak boleh kosong!',
                            },
                        ]}
                    >
                        <InputNumber placeholder="Input Stock"  style={{ width: '100%' }}/>
                    </Form.Item>
                    <Form.Item
                        name="price"
                        label="Price"
                        rules={[
                            {
                                required: true,
                                message: 'Price tidak boleh kosong!',
                            },
                        ]}
                    >
                        <InputNumber placeholder="Input Price" style={{ width: '100%' }} />
                    </Form.Item>
                    {/*<Form.Item*/}
                    {/*    name="photo"*/}
                    {/*    label="Photo"*/}
                    {/*>*/}
                    {/*    <Upload*/}
                    {/*        name="avatar"*/}
                    {/*        listType="picture-card"*/}
                    {/*        className="avatar-uploader"*/}
                    {/*        showUploadList={false}*/}
                    {/*        action="https://www.mocky.io/v2/5cc8019d300000980a055e76"*/}
                    {/*        beforeUpload={beforeUpload}*/}
                    {/*        onChange={handleChange}*/}
                    {/*    >*/}
                    {/*        {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}*/}
                    {/*    </Upload>*/}
                    {/*</Form.Item>*/}
                    <Form.Item
                        name="photo"
                        label="Photo"
                        rules={[
                            {
                                required: true,
                                message: 'Photo tidak boleh kosong!',
                            },
                        ]}
                    >
                        <Input placeholder="Input Photo Url" />
                    </Form.Item>

                    <Form.Item>
                        <Button type="primary" onClick={async () => {
                            Modal.confirm({
                                title: 'Peringatan!',
                                icon: <ExclamationCircleOutlined />,
                                content: 'Apakah Anda yakin ingin menyimpan data ini ?',
                                okText: 'Yakin',
                                cancelText: 'Batal',
                                onOk: async () => {
                                    try {
                                        const values = await form.validateFields();
                                        form.setFieldsValue({});
                                        const data = {
                                            name: values.name,
                                            category: values.category,
                                            stock: values.stock,
                                            price: values.price,
                                            photo: values.photo || '',
                                        };
                                        console.log('data insert 2', data);
                                        await store.product.insertProduct(data);
                                        history.goBack();
                                        notification.success({
                                            message: 'Berhasil',
                                            description:
                                                'Data Biaya berhasil didaftarkan',
                                        });
                                    } catch (err) {
                                        console.log('error product post men', err);
                                    }
                                },
                                onCancel() {
                                    console.log('Cancel');
                                },
                            });
                        }}>Submit</Button>
                    </Form.Item>
                </Form>
            </div>
        </Card>
    </div>
});
