import React, {useEffect, useState} from "react";
import {Avatar, Button, Card, Col, PageHeader, Row, Space, Table, Modal, InputNumber, Form, notification} from "antd";
import {observer} from "mobx-react-lite";
import {useStore} from "../../utils/useStore";
import {ExclamationCircleOutlined, ShoppingCartOutlined} from '@ant-design/icons';

const {Meta} = Card;

export const MenuList = observer(() => {
    const store = useStore();
    const [data, setData] = useState([]);
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [productId, setProductId] = useState('');
    const [initialData] = useState({});
    const [form] = Form.useForm();

    const setFieldsValue = () => {
        form.setFieldsValue({
            qty: '',
        });
    };

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        Modal.confirm({
            title: 'Peringatan!',
            icon: <ExclamationCircleOutlined />,
            content: 'Apakah Anda yakin ingin menyimpan data ini ?',
            okText: 'Yakin',
            cancelText: 'Batal',
            onOk: async () => {
                try {
                    const values = await form.validateFields();
                    form.setFieldsValue({});
                    const data = {
                        productId: productId,
                        quantity: values.qty,
                    };
                    await store.order.insertCart(data);
                    console.log('data insert cart', data);
                    setIsModalVisible(false);
                    notification.success({
                        message: 'Berhasil',
                        description:
                            'Cart berhasil didaftarkan',
                    });

                } catch (err) {
                    console.log('error cart men', err);
                }
            },
            onCancel() {
                console.log('Cancel');
            },
        });


    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    useEffect(() => {
        const init = async () => {
            await store.product.getProduct();
        };
        init();
        setFieldsValue();
    }, []);

    const datas = [
        {
            name: 'Burger Sapi',
            photo: 'https://cdn-2.tstatic.net/manado/foto/bank/images/resep-burger-sapi-34738.jpg',
            category: 'Food',
            stock: '20',
        },
        {
            name: 'HotDog',
            photo: 'http://kbu-cdn.com/dk/wp-content/uploads/spicy-hot-dog.jpg',
            category: 'Food',
            stock: '20',
        },
        {
            name: 'Soda Gembira',
            photo: 'https://www.resepkuerenyah.com/wp-content/uploads/2015/08/Cara-Membuat-Es-Soda-Gembira-Segar-dan-Nikmat.jpg',
            category: 'Drink',
            stock: '20',
        },
        {
            name: 'Ice Tea',
            photo: 'https://www.unileverfoodsolutions.co.id/dam/global-ufs/mcos/SEA/calcmenu/recipes/ID-recipes/desserts-&-bakery/bango-ice-lime-tea/main-header.jpg',
            category: 'Drink',
            stock: '20',
        },
        {
            name: 'Espresso',
            photo: 'https://www.illy.com/content/dam/channels/website/consumer/global/recipies/Doppio-tazza-media_mobile.jpg',
            category: 'Drink',
            stock: '20',
        },
    ]

    return <div>
        <PageHeader
            style={{
                padding: 0,
                margin: 0,
                height: 40,
                backgroundColor: "transparent",
            }}
            title={"Menu"}
        >
        </PageHeader>
        <div>
            <Row>
                {store.product.data.map(it => {
                        return (
                            <Col span={8}>
                                <Card
                                    style={{width: 300, marginBottom: 40}}
                                    cover={
                                        <img
                                            style={{
                                                height:200
                                            }}
                                            alt="example"
                                            src={it.photo}
                                        />
                                    }
                                    actions={[
                                        <Button type="link" onClick={()=> {
                                            console.log('coy id',it.id)
                                            setIsModalVisible(true)
                                            setProductId(it.id);
                                        }}>Pilih</Button>
                                    ]}
                                >
                                    <Meta
                                        title={it.name}
                                        description={it.category}
                                    />
                                </Card>
                            </Col>
                        )
                    }
                )}
            </Row>
        </div>
        <Modal title="Masukan Stock" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
            <Form
                layout={'vertical'}
                form={form}
                initialValues={initialData}
            >
                <Form.Item
                    name="qty"
                    label="Quantity"
                    rules={[
                        {
                            required: true,
                            message: 'Quantity tidak boleh kosong!',
                        },
                    ]}
                >
                    <InputNumber
                        placeholder="10"
                        style={{ width: '100%' }}
                    />
                </Form.Item>
            </Form>
        </Modal>
    </div>
});
