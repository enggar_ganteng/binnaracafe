import React, {useState, useEffect} from "react";
import {Button, PageHeader, Space, Table, Tag} from "antd";
import {observer} from "mobx-react-lite";
import {useStore} from "../../utils/useStore";
import {http} from "../../utils/http";

export const Kiai = observer(() => {
    const store = useStore();
    const [data, setData] = useState([]);

    useEffect(() => {
        const init = async () => {
          const response = await http.get("/tanya-kiai/v1/kiai").set("X-Admin-Token", "2c7560f9-a42c-4645-bcad-211ef433ea2c");
          setData(response.body.data);
        };
        init();
    }, []);

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: text => <a>{text}</a>,
        },
        {
            title: 'Photo',
            dataIndex: 'photo',
            key: 'photo',
            render: (text, data) => <img style={{height: 50, width: 50}} src={data.photo} alt={"yo"}/>,
        },
        {
            title: 'Location',
            dataIndex: 'location',
            key: 'location',
            render: (text, data) => {
                return data.location.name;
            }
        },
        // {
        //     title: 'Education',
        //     dataIndex: 'education',
        //     key: 'education',
        // },
        // {
        //     title: 'University',
        //     dataIndex: 'education_place_name',
        //     key: 'education_place_name',
        // },
        // {
        //     title: 'Major',
        //     dataIndex: 'education_major',
        //     key: 'education_major',
        // },
        // {
        //     title: 'Occupation',
        //     dataIndex: 'occupation',
        //     key: 'occupation',
        // },
        // {
        //     title: 'Organization',
        //     dataIndex: 'organization',
        //     key: 'organization',
        // },
        {
            title: 'Speciality',
            key: 'speciality',
            dataIndex: 'speciality',
            render: speciality => (
                <>
                    {(speciality ?? []).map(speciality => {
                        // let color = speciality.length > 5 ? 'geekblue' : 'green';
                        return (
                            <Tag color={'green'} key={speciality.id}>
                                {speciality.name}
                            </Tag>
                        );
                    })}
                </>
            ),
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
            render: (text, record) => {
                return <Tag color={record.status === 'active' ? 'green' : 'yellow'}>
                    {text}
                </Tag>
            }
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <Space size="middle">
                    {record.status === 'waiting_validation' && <a>Approve</a>}

                </Space>
            ),
        },
    ];

    // const data = [
    //     {
    //         key: '1',
    //         name: 'John Brown',
    //         age: 32,
    //         address: 'New York No. 1 Lake Park',
    //         tags: ['nice', 'developer'],
    //     },
    //     {
    //         key: '2',
    //         name: 'Jim Green',
    //         age: 42,
    //         address: 'London No. 1 Lake Park',
    //         tags: ['loser'],
    //     },
    //     {
    //         key: '3',
    //         name: 'Joe Black',
    //         age: 32,
    //         address: 'Sidney No. 1 Lake Park',
    //         tags: ['cool', 'teacher'],
    //     },
    // ];


    return <div>
        <PageHeader
            style={{
                padding: 0,
                margin: 0,
                height: 40,
                backgroundColor: "transparent",
            }}
            title={"Kiai"}
        >
        </PageHeader>
        <Table columns={columns} dataSource={data} />
        {/*{store.channel.channels.map(it => {*/}
        {/*    return <div key={it.id}>{it.message}</div>*/}
        {/*})}*/}
        {/*<Button onClick={() => {*/}
        {/*    store.channel.tesuto();*/}
        {/*}}>yo dayo</Button>*/}

        {/*<Button onClick={() => {*/}
        {/*    setYo('a');*/}
        {/*}}>update</Button>*/}
    </div>
});
