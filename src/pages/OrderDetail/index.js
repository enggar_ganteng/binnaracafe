import React, {useEffect, useState} from "react";
import {Avatar, Button, Card, Col, PageHeader, Row, Space, Table, List, InputNumber, Divider, Typography, Modal} from "antd";
import {observer} from "mobx-react-lite";
import {useStore} from "../../utils/useStore";
import {DeleteOutlined, ExclamationCircleOutlined} from '@ant-design/icons';
import NumberFormat from "react-number-format";
import { useHistory } from "react-router-dom";

const { Title } = Typography;
const {Meta} = Card;

export const OrderDetail = observer(() => {
    const store = useStore();
    const history = useHistory();
    const [data, setData] = useState([]);

    useEffect(() => {
        const init = async () => {

        };
        init();
    }, []);

    return <div>
        <PageHeader
            style={{
                padding: 0,
                margin: 0,
                height: 40,
                backgroundColor: "transparent",
            }}
            title={"Order Detail"}
        >
        </PageHeader>
        <Card title={"Transaksi 00001112020"}>
            <div>
                <div style={{display:"flex", flexDirection:'row', justifyContent:'space-between', marginBottom:10}}>
                    <div>
                        Burger x7
                    </div>
                    <div>
                        <NumberFormat
                            value={140000}
                            displayType={'text'}
                            thousandSeparator={"."}
                            decimalSeparator={","}
                            prefix={'Rp. '}
                            renderText={value => <div>{value}</div>}/>
                    </div>
                </div>
                <div style={{display:"flex", flexDirection:'row', justifyContent:'space-between', marginBottom:10}}>
                    <div>
                        HotDog x2
                    </div>
                    <div>
                        <NumberFormat
                            value={20000}
                            displayType={'text'}
                            thousandSeparator={"."}
                            decimalSeparator={","}
                            prefix={'Rp. '}
                            renderText={value => <div>{value}</div>}/>
                    </div>
                </div>
                <div style={{display:"flex", flexDirection:'row', justifyContent:'space-between', marginBottom:10}}>
                    <div>
                        Soda Gembira x5
                    </div>
                    <div>
                        <NumberFormat
                            value={25000}
                            displayType={'text'}
                            thousandSeparator={"."}
                            decimalSeparator={","}
                            prefix={'Rp. '}
                            renderText={value => <div>{value}</div>}/>
                    </div>
                </div>
                <div style={{display:"flex", flexDirection:'row', justifyContent:'space-between', marginBottom:10}}>
                    <div>
                        Ice Tea x3
                    </div>
                    <div>
                        <NumberFormat
                            value={39000}
                            displayType={'text'}
                            thousandSeparator={"."}
                            decimalSeparator={","}
                            prefix={'Rp. '}
                            renderText={value => <div>{value}</div>}/>
                    </div>
                </div>
                <div style={{display:"flex", flexDirection:'row', justifyContent:'space-between', marginBottom:10}}>
                    <div>
                        Espresso x7
                    </div>
                    <div>
                        <NumberFormat
                            value={70000}
                            displayType={'text'}
                            thousandSeparator={"."}
                            decimalSeparator={","}
                            prefix={'Rp. '}
                            renderText={value => <div>{value}</div>}/>
                    </div>
                </div>
            </div>
            <Divider />
            <div>
                <div style={{display:"flex", flexDirection:'row', justifyContent:'space-between', marginBottom:10}}>
                    <div>
                        <Title level={5} strong>Total Harga</Title>
                    </div>
                    <div>
                        <NumberFormat
                            value={294000}
                            displayType={'text'}
                            thousandSeparator={"."}
                            decimalSeparator={","}
                            prefix={'Rp. '}
                            renderText={value => <div>{value}</div>}/>
                    </div>
                </div>
            </div>
        </Card>
    </div>
});
