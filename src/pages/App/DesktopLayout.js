import React, {useState} from "react";
import {Button, Layout, Menu, PageHeader, Popover, Typography} from "antd";
import {MenuList} from "./MenuList";
import {Link, useHistory} from "react-router-dom";
import {UserOutlined, ShoppingCartOutlined} from "@ant-design/icons";
import {AppRoute} from "../../routes/app";
import Logo from '../../assets/logo.png';
import LogoNew from '../../assets/logo_new.png';
import {useStore} from "../../utils/useStore";

const {Text, Paragraph} = Typography;
const {Header, Content, Sider} = Layout;

export const DesktopLayout = () => {
    const store = useStore();
    const history = useHistory();
    const [clicked, setClicked] = useState(false);
    const [clickedCard, setClickedCard] = useState(false);

    return <Layout
        theme={"light"}
        className={"transparent"}
        hasSider={true}
        style={{
            paddingLeft: 0,
            display: "flex",
            width: "100vw",
            height: "100vh",
        }}>

        <Sider
            className={"transparent"}
            width={240}
            style={{
                overflowX: "hidden",
                bottom: 0,
                justifyContent: "flex-start",
                paddingTop: 20,
                paddingLeft: 20,
                position: "fixed",
                top: 0,
                zIndex: 10,
            }}
        >
            <div style={{
                paddingLeft: 20,
                marginBottom: 40,
            }}>
                <img src={LogoNew} alt="logo" style={{
                    width: 100,
                    // height:50
                }}/>
            </div>
            <MenuList closeLeftDrawer={() => {
            }}/>
        </Sider>


        <Layout style={{
            paddingLeft: 240
        }}>
            <Header
                theme={"light"}
                style={{
                    height: 54,
                    paddingLeft: 0,
                    paddingRight: 0,
                    backgroundColor: "transparent",
                    maxWidth: 1024,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'flex-end'
                }}
            >
                <Popover
                    autoAdjustOverflow={true}
                    placement="bottom"
                    content={
                        <Menu
                            type={"line"}
                            inlineIndent={0}
                            theme="light"
                            style={{
                                backgroundColor: "transparent",
                                borderRightWidth: 0,
                            }}
                            mode="inline"
                        >
                            <Menu.Item
                                onClick={() => {

                                }}
                            >
                                <div style={{
                                    display:'flex',
                                    flexDirection:'row',
                                    justifyContent:'center',
                                }}>
                                    <Link to={'/app/cart'}>
                                        <span>See All</span>
                                    </Link>
                                </div>
                            </Menu.Item>
                        </Menu>
                    }
                    title={
                        <Text>
                            admin@binnara-cafe.id
                            <Paragraph style={{fontWeight: 400}} type={"secondary-dark"}>
                                Administrator
                            </Paragraph>
                        </Text>
                    }
                    trigger="click"
                    visible={clickedCard}
                    onVisibleChange={() => setClickedCard(!clickedCard)}
                >
                    <Button
                        size={"default"}
                        style={{marginRight:30}}
                        icon={
                            <ShoppingCartOutlined style={{fontSize: "13px"}}/>
                        }
                    />
                </Popover>
                <Popover
                    autoAdjustOverflow={true}
                    placement="bottomRight"
                    content={
                        <Menu
                            type={"line"}
                            inlineIndent={0}
                            theme="light"
                            style={{
                                backgroundColor: "transparent",
                                borderRightWidth: 0,
                            }}
                            mode="inline"
                        >
                            <Menu.Item
                                onClick={() => {
                                    store.authentication.logout();
                                    return history.push("/login");
                                }}
                            >
                                <span>Sign out</span>
                            </Menu.Item>
                        </Menu>
                    }
                    title={
                        <Text>
                            admin@binnara-cafe.id
                            <Paragraph style={{fontWeight: 400}} type={"secondary-dark"}>
                                Administrator
                            </Paragraph>
                        </Text>
                    }
                    trigger="click"
                    visible={clicked}
                    onVisibleChange={() => setClicked(!clicked)}
                >
                    <Button
                        size={"default"}
                        style={{}}
                        icon={
                            <UserOutlined style={{fontSize: "13px"}}/>
                        }
                    />
                </Popover>
            </Header>
            <Content style={{
                maxWidth: 1024,
            }}>
                <AppRoute/>
            </Content>
        </Layout>
    </Layout>
};
