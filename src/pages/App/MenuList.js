import React, {useEffect, useState} from "react";
import {Menu} from "antd";
import {Link} from "react-router-dom";
import {HistoryOutlined, HomeOutlined, CoffeeOutlined, DropboxOutlined} from "@ant-design/icons";
import {observer} from "mobx-react-lite";
import {useStore} from "../../utils/useStore";

const {SubMenu} = Menu;

export const MenuList = observer((props) => {
	const store = useStore();
	useEffect(() => {
	}, []);

	const [setKeys, setSetKeys] = useState(["dashboard"]);

	return (
		<Menu
			defaultOpenKeys={["sub4"]}
			theme="light"
			style={{
				backgroundColor: "transparent",
				borderRightWidth: 0,
				fontWeight: 400,
				paddingLeft: 0,
			}}
			onClick={({keyPath, item}) => {
				props.closeLeftDrawer();
			}}
			mode="inline"
			selectedKeys={setKeys}
			onSelect={({setKeys, item, selectedKeys}) => setSetKeys(selectedKeys)}
			overflowedIndicator={0}
			forceSubMenuRender={true}
		>
			<Menu.Item key="home">
				<Link to={'/app/home'}>
					<HomeOutlined/>
					<span>Home</span>
				</Link>
			</Menu.Item>

			<Menu.Item key="order">
				<Link to={'/app/order'}>
					<HistoryOutlined />
					<span>Order</span>
				</Link>
			</Menu.Item>
			{localStorage.getItem('id_token') === 'kasir' && (
				<Menu.Item key="menu">
					<Link to={'/app/menu'}>
						<CoffeeOutlined />
						<span>Menu</span>
					</Link>
				</Menu.Item>
			)}
			{localStorage.getItem('id_token') === 'admin' && (
			<Menu.Item key="product">
				<Link to={'/app/product'}>
					<DropboxOutlined />
					<span>Product</span>
				</Link>
			</Menu.Item>
			)}
			<Menu.Divider style={{background: "transparent", paddingTop: 15}}/>
		</Menu>
	);
});
