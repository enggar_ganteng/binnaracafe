import React, {useEffect, useState} from "react";
import {Avatar, Button, Card, Col, PageHeader, Row, Space, Table, List, InputNumber, Divider, Typography, Modal} from "antd";
import {observer} from "mobx-react-lite";
import {useStore} from "../../utils/useStore";
import {DeleteOutlined, ExclamationCircleOutlined} from '@ant-design/icons';
import NumberFormat from "react-number-format";
import { useHistory } from "react-router-dom";

const { Title } = Typography;
const {Meta} = Card;

export const Cart = observer(() => {
    const store = useStore();
    const history = useHistory();
    const [data, setData] = useState([]);

    useEffect(() => {
        const init = async () => {
            const init = async () => {
                await store.order.getCart();
            };
            init();
        };
        init();
    }, []);

    const datas = [
        {
            name: 'Burger Sapi',
            photo: 'https://cdn-2.tstatic.net/manado/foto/bank/images/resep-burger-sapi-34738.jpg',
            category: 'Food',
            price:20000,
            qty: 1,
        },
        {
            name: 'HotDog',
            photo: 'http://kbu-cdn.com/dk/wp-content/uploads/spicy-hot-dog.jpg',
            category: 'Food',
            price:10000,
            qty: 2,
        },
        {
            name: 'Soda Gembira',
            photo: 'https://www.resepkuerenyah.com/wp-content/uploads/2015/08/Cara-Membuat-Es-Soda-Gembira-Segar-dan-Nikmat.jpg',
            category: 'Drink',
            price:5000,
            qty: 5,
        },
        {
            name: 'Ice Tea',
            photo: 'https://www.unileverfoodsolutions.co.id/dam/global-ufs/mcos/SEA/calcmenu/recipes/ID-recipes/desserts-&-bakery/bango-ice-lime-tea/main-header.jpg',
            category: 'Drink',
            price:13000,
            qty: 3,
        },
        {
            name: 'Espresso',
            photo: 'https://www.illy.com/content/dam/channels/website/consumer/global/recipies/Doppio-tazza-media_mobile.jpg',
            category: 'Drink',
            price:10000,
            qty: 7,
        },
    ]


    return <div>
        <PageHeader
            style={{
                padding: 0,
                margin: 0,
                height: 40,
                backgroundColor: "transparent",
            }}
            title={"Cart"}
            extra={[
                <Button key="1" type="primary" onClick={()=> {
                    Modal.confirm({
                        title: 'Konfirmasi',
                        icon: <ExclamationCircleOutlined />,
                        content: 'Apakah pembayarannnya sudah benar ?',
                        okText: 'Ya',
                        onOk: () => {
                            history.push('order/00001112020')
                        },
                        cancelText: 'Tidak',
                    });
                }}>
                    Bayar
                </Button>,
            ]}
        >
        </PageHeader>
        <Card style={{marginBottom:40}}>
            <List
                itemLayout="horizontal"
                dataSource={store.order.dataCart}
                renderItem={item => (
                    <List.Item
                        actions={[<InputNumber disabled={true} defaultValue={item.quantity}  onChange={()=> {

                        }} />,<Button type="primary" icon={<DeleteOutlined />} shape="circle" />,
                            <Button key="1" type="primary" onClick={()=> {
                                Modal.confirm({
                                    title: 'Konfirmasi',
                                    icon: <ExclamationCircleOutlined />,
                                    content: 'Apakah pembayarannnya sudah benar ?',
                                    okText: 'Ya',
                                    onOk: () => {
                                        store.order.checkoutCart(item.id)
                                        console.log('success checkoutCart')
                                    },
                                    cancelText: 'Tidak',
                                });
                            }}>
                                Bayar
                            </Button>
                        ]}
                    >
                        <List.Item.Meta
                            avatar={<Avatar src={item.products.photo} />}
                            title={<a href="#">{item.products.name}</a>}
                            description={<NumberFormat
                                value={item.products.price}
                                displayType={'text'}
                                thousandSeparator={"."}
                                decimalSeparator={","}
                                prefix={'Rp. '}
                                renderText={value => <div>{value}</div>}/>}
                        />
                    </List.Item>
                )}
            />
        </Card>
        <Card title={"Ringkasan Pembayaran"}>
            <div>
                {store.order.dataCart.map(item => {
                    return(
                        <div style={{display:"flex", flexDirection:'row', justifyContent:'space-between', marginBottom:10}}>
                            <div>
                                {item.products.name} x{item.quantity}
                            </div>
                            <div>
                                <NumberFormat
                                    value={item.products.price * item.quantity}
                                    displayType={'text'}
                                    thousandSeparator={"."}
                                    decimalSeparator={","}
                                    prefix={'Rp. '}
                                    renderText={value => <div>{value}</div>}/>
                            </div>
                        </div>
                    )
                })}
            </div>
            <Divider />
            <div>
                <div style={{display:"flex", flexDirection:'row', justifyContent:'space-between', marginBottom:10}}>
                    <div>
                        <Title level={5} strong>Total Harga</Title>
                    </div>
                    <div>
                        <NumberFormat
                            value={294000}
                            displayType={'text'}
                            thousandSeparator={"."}
                            decimalSeparator={","}
                            prefix={'Rp. '}
                            renderText={value => <div>{value}</div>}/>
                    </div>
                </div>
            </div>
        </Card>
    </div>
});
