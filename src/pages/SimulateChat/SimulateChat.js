import React, {useEffect, useState} from "react";
import {Button, Form, Input, PageHeader, Select, List, Typography} from "antd";
import {useStore} from "../../utils/useStore";
import {observer, Observer} from "mobx-react-lite";
import {UserOutlined} from "@ant-design/icons";

export const SimulateChat = observer(() => {
    const [form] = Form.useForm();
    const store = useStore();
    const [message, setMessage] = useState('');
    const [render, setRender] = useState('');

    useEffect(() => {
        // console.log("update yo ", store);
    }, [store]);

    useEffect(() => {
        const init = async () => {
            store.chat.initialize();
            store.chat.connect();
            // store.chat
            // await store.channel.getChannel();
        };
        setInterval(() => {
            setRender(Math.random().toString())
            console.log("render", render);
        }, 1000);
        init();
    }, []);

    return <div>
        <PageHeader
            style={{
                padding: 0,
                margin: 0,
                height: 40,
                backgroundColor: "transparent",
            }}
            title={"Simulate Chat"}
        >
        </PageHeader>

        <div style={{display: 'none'}}>{render}</div>
        <div>
            <List
                bordered
                dataSource={store.chat.chatMessages.slice()}
                style={{
                    backgroundColor: 'white',
                    marginBottom: 16
                }}
                renderItem={item => (
                    <List.Item key={item.id}>
                        {item.message}
                    </List.Item>
                )}
            />
        </div>

        <Form
            form={form}
            layout={"vertical"}
            onValuesChange={(values) => {
                // console.log("values", values);
            }}
        >
            {/*<Form.Item label="Channel">*/}
            {/*    <Select>*/}
            {/*        {store.channel.channels.map(channel => {*/}
            {/*            return <Select.Option value={channel.id}>{channel.id}</Select.Option>*/}
            {/*        })}*/}
            {/*    </Select>*/}
            {/*</Form.Item>*/}
            {/*<Form.Item label="As">*/}
            {/*    <Select>*/}
            {/*        <Select.Option value={"kiai"}>Kiai</Select.Option>*/}
            {/*        <Select.Option value={"user"}>User</Select.Option>*/}
            {/*    </Select>*/}
            {/*</Form.Item>*/}

            <Form.Item>
                <Input
                    type="text"
                    value={message}
                    onChange={(event) => {
                        setMessage(event.target.value);
                        console.log("text", event.target.value);
                    }}
                    placeholder="Message" style={{
                        marginBottom: 8
                }}/>

                <Button type="primary" onClick={async () => {
                    await store.chat.sendMessage(message);
                    setMessage('');
                    console.log("on");
                }}>
                    Send message
                </Button>
            </Form.Item>
        </Form>
    </div>
});
