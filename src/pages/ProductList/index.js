import React, {useState, useEffect} from "react";
import {Button, Modal, notification, PageHeader, Space, Table, Tag} from "antd";
import {observer} from "mobx-react-lite";
import {useStore} from "../../utils/useStore";
import {http} from "../../utils/http";
import {Link} from "react-router-dom";
import NumberFormat from "react-number-format";
import {ExclamationCircleOutlined} from "@ant-design/icons";

export const ProductList = observer(() => {
    const store = useStore();
    const [data, setData] = useState([]);

    useEffect(() => {
        const init = async () => {
            await store.product.getProduct();
        };
        init();
    }, []);

    const datas = [
        {
            name:'Burger Sapi',
            photo:'https://cdn-2.tstatic.net/manado/foto/bank/images/resep-burger-sapi-34738.jpg',
            category:'Food',
            stock:'20',
        },
        {
            name:'HotDog',
            photo:'http://kbu-cdn.com/dk/wp-content/uploads/spicy-hot-dog.jpg',
            category:'Food',
            stock:'20',
        },
        {
            name:'Soda Gembira',
            photo:'https://www.resepkuerenyah.com/wp-content/uploads/2015/08/Cara-Membuat-Es-Soda-Gembira-Segar-dan-Nikmat.jpg',
            category:'Drink',
            stock:'20',
        },
        {
            name:'Ice Tea',
            photo:'https://www.unileverfoodsolutions.co.id/dam/global-ufs/mcos/SEA/calcmenu/recipes/ID-recipes/desserts-&-bakery/bango-ice-lime-tea/main-header.jpg',
            category:'Drink',
            stock:'20',
        },
        {
            name:'Espresso',
            photo:'https://www.illy.com/content/dam/channels/website/consumer/global/recipies/Doppio-tazza-media_mobile.jpg',
            category:'Drink',
            stock:'20',
        },
    ]

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: text => <a>{text}</a>,
        },
        {
            title: 'Photo',
            dataIndex: 'photo',
            key: 'photo',
            render: (text, data) => <img style={{height: 50, width: 50}} src={data.photo} alt={"photo"}/>,
        },
        {
            title: 'Stock',
            dataIndex: 'stock',
            key: 'stock',
            render: text => <p>{text}</p>,
        },

        {
            title: 'Category',
            key: 'category',
            dataIndex: 'category',
            render: text => <p>{text}</p>,
        },
        {
            title: 'Price',
            key: 'price',
            dataIndex: 'price',
            render: text => <NumberFormat
                value={text}
                displayType={'text'}
                thousandSeparator={"."}
                decimalSeparator={","}
                prefix={'Rp. '}
                renderText={value => <div>{value}</div>}/>,
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <Space size="middle">
                    <Button type="dashed" danger onClick={()=> {
                        console.log('record', record);

                        Modal.confirm({
                            title: 'Peringatan!',
                            icon: <ExclamationCircleOutlined />,
                            content: 'Apakah Anda yakin ingin menghapus data ini ?',
                            okText: 'Yakin',
                            cancelText: 'Batal',
                            onOk: async () => {
                                store.product.deleteProduct(record.id);
                            },
                            onCancel() {
                                console.log('Cancel');
                            },
                        });

                    }}>
                        Delete
                    </Button>
                </Space>
            ),
        }
    ];

    return <div>
        <PageHeader
            style={{
                padding: 0,
                margin: 0,
                height: 40,
                backgroundColor: "transparent",
            }}
            title={"Product"}
            extra={[
                <Link to={'/app/product/form'}>
                    <Button key="1" type="primary">
                        Create
                    </Button>
                </Link>,
            ]}
        >
        </PageHeader>
        <Table columns={columns} dataSource={store.product.data} />
    </div>
});
