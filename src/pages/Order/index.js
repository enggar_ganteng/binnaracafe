import React, {useState, useEffect} from "react";
import {Button, PageHeader, Space, Table, Tag} from "antd";
import {observer} from "mobx-react-lite";
import {useStore} from "../../utils/useStore";
import {http} from "../../utils/http";
import moment from "moment";
import {EyeOutlined} from "@ant-design/icons";
import NumberFormat from "react-number-format";
import {useHistory} from "react-router-dom";
import {Link} from "react-router-dom";

export const Order = observer(() => {
    const store = useStore();
    const history = useHistory();
    const [data, setData] = useState([]);

    useEffect(() => {
        const init = async () => {
            await store.order.getOrder();
        };
        init();
    }, []);

    const datas = [
        {
            no_order:'00001112020',
            kasir:'kasir@binnaracafe.id',
            tanggal: new Date(),
            total_price:300000,
        },
        {
            no_order:'00001212020',
            kasir:'kasir@binnaracafe.id',
            tanggal: new Date(),
            total_price:250000,
        },
    ]

    const columns = [
        {
            title: 'No. Order',
            dataIndex: 'id',
            key: 'id',
            render: text => <a>{text}</a>,
        },
        {
            title: 'Kasir',
            dataIndex: 'kasir',
            key: 'kasir',
            render: text => <div>kasir@binnaracafe.id</div>,
        },
        {
            title: 'Tanggal',
            dataIndex: 'tanggal',
            key: 'tanggal',
            render: text => <div>{moment(text).format('MMMM YYYY')}</div>,
        },
        {
            title: 'Total Harga',
            render: (text,record) => <div>
                <NumberFormat
                    value={record.quantity * record.products.price}
                    displayType={'text'}
                    thousandSeparator={"."}
                    decimalSeparator={","}
                    prefix={'Rp. '}
                    renderText={value => <div>{value}</div>}/>
            </div>,
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <Space size="middle">
                    <Link to={`/app/order/${record.id}`}>
                        <Button icon={<EyeOutlined />} shape="circle" />
                    </Link>
                </Space>
            ),
        },
    ];

    return <div>
        <PageHeader
            style={{
                padding: 0,
                margin: 0,
                height: 40,
                backgroundColor: "transparent",
            }}
            title={"Order"}
        >
        </PageHeader>
        <Table columns={columns} dataSource={store.order.dataOrder} />
    </div>
});
