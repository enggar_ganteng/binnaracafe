job "${CI_PROJECT_NAME}" {
  datacenters = ["dc1"]

  vault {
    policies = ["kesan"]
  }

  group "${CI_PROJECT_NAME}" {
    count = 1

    network {
      port "http" {
        to = 80
      }
    }


    service {
      name = "${CI_PROJECT_NAME}"
      port = "http"
      tags = [
        "traefik.enable=true",
        "traefik.http.routers.${CI_PROJECT_NAME}.rule=Host(`kesan-cms.bangun-kreatif.com`)",
      ]
      check {
        port = "http"
        type = "http"
        path = "/"
        interval = "2s"
        timeout = "2s"
      }
    }

    task "server" {
      driver = "docker"
      config {
        image = "${DOCKER_REGISTRY}/${DOCKER_IMAGE}:${CI_COMMIT_SHORT_SHA}"
        force_pull = true
        ports = ["http"]
        auth {
          username = "${USER_GITLAB_DEVOPS}"
          password = "${PASSWORD_GITLAB_DEVOPS}"
        }
      }
      env {
      }
    }
  }
}
